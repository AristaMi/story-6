from django.shortcuts import render
from .forms import FormStory
from .models import ModelStory
from django.utils import timezone

# Create your views here.
def index(request):
    modelStory = ModelStory.objects.all().order_by("-time")
    context = {
        'hasilStory' : modelStory
    }

    if request.method == 'POST':
        form = FormStory(request.POST)
        if form.is_valid():
            ShareStory = request.POST['ShareStory']
            time = timezone.now()
            addStory = ModelStory(ShareStory=ShareStory, time=time)
            addStory.save()
#    return redirect('/testimony/')
    return render(request, 'Landing Page.html', context)
from django import forms
from .models import ModelStory

class FormStory(forms.ModelForm):
    class Meta(object):
        model = ModelStory
        fields = [
            'ShareStory'
        ]
        
from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story6UnitTest(TestCase):

	def test_mengecek_halaman_LandingPage_jalan(self):
		c = Client()
		response = c.get("")
		content = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)

	def test_mengecek_ada_tulisan_Lets_Spread_Your_Story(self):
		request = HttpRequest()
		c = Client()
		response = c.get("")
		content = response.content.decode('utf8')
		self.assertIn("Let's Spread Your Story", content)

	def test_apakah_ada_button(self):
		c = Client()
		response = c.get('')
		content = response.content.decode('utf8')
		self.assertIn('<button', content)

	def test_apakah_ada_table(self):
		c = Client()
		response = c.get('')
		content = response.content.decode('utf8')
		self.assertIn('<table', content)

	def test_apa_menggunakan_template_Landing_Page(self):
		c = Client()
		response = c.get('')
		self.assertTemplateUsed(response, 'Landing Page.html')


class Story6FunctionalTest(TestCase):

 	def setUp(self):
 		chrome_options = Options()
 		chrome_options.add_argument('--dns-prefetch-disable')
 		chrome_options.add_argument('--no-sandbox')        
 		chrome_options.add_argument('--headless')
 		chrome_options.add_argument('disable-gpu')
 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
 		super(Story6FunctionalTest, self).setUp()

 	def tearDown(self):
 		self.selenium.quit()
 		super(Story6FunctionalTest, self).tearDown()

 	def test_input_todo(self):
 		selenium = self.selenium
 		# Opening the link we want to test
 		selenium.get('http://127.0.0.1:8000/')
 		# find the form element
 		curahan = selenium.find_element_by_id('ShareStory')
 		submit = selenium.find_element_by_id('submit')

 		# Fill the form with data
 		curahan.send_keys('Coba coba')

		# submitting the form
 		submit.send_keys(Keys.RETURN)
